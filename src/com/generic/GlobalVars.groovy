#!/usr/bin/env groovy
package com.generic

class GlobalVars {
    static String defaultName = "Foo"

    static final String SLACK_MESSAGE = "Sending Slack Notification"
    static final String EMAIL_MESSAGE = "Sending Email"
}

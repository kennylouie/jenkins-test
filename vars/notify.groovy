import com.generic.GlobalVars

def call(Map config=[:]) {
    if (config.type == "slack") {
        echo GlobalVars.SLACK_MESSAGE
    } else {
        echo GlobalVars.EMAIL_MESSAGE
    }
    echo config.message
}

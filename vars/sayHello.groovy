#!/usr/bin/env groovy

import com.generic.GlobalVars

def call(String name = GlobalVars.defaultName) {
    echo "Hello, ${name}."
}
